import { NestFactory } from '@nestjs/core';
import { ExpressAdapter } from '@nestjs/platform-express';
import * as express from 'express';
import * as functions from 'firebase-functions';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import * as cookieParser from 'cookie-parser';
import * as admin from 'firebase-admin';

const server = express();
export const createNestServer = async expressInstance => {
  const app = await NestFactory.create(
    AppModule,
    new ExpressAdapter(expressInstance),
  );
  app.enableCors({ origin: true, credentials: true });
  app.useGlobalPipes(new ValidationPipe());
  app.use(cookieParser());
  admin.initializeApp(functions.config().firebase);
  return app.init();
};

export const api = functions.https.onRequest(
  async (req, res) => {
    await createNestServer(server);
    server(req, res);
  });
