import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import * as cookieParser from 'cookie-parser';
import * as admin from 'firebase-admin';
import * as fs from 'fs';
import * as path from 'path';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors({ origin: true, credentials: true });
  app.useGlobalPipes(new ValidationPipe());
  app.use(cookieParser());

  const serviceAccount = fs.readFileSync(
    path.resolve('./service-account.json'),
    'utf-8',
  );
  admin.initializeApp({
    credential: admin.credential.cert(JSON.parse(serviceAccount)),
  });

  await app.listen(3100);
}
bootstrap();
